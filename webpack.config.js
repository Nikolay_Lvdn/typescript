const path = require('path');

module.exports = {
    resolve: {
        extensions: ['.js', '.ts']
    },
    entry: './index.ts',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'bundle.js',
        publicPath: '/dist/'
    },
    module: {
        rules: [{
                test: /\.ts$/,
                loader: 'ts-loader'
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            }
        ]
    },
    mode: 'development',
    devServer: {
        inline: true
    },
    devtool: "source-map"
}