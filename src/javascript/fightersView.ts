import { createFighter } from './fighterView';
import { showFighterDetailsModal } from './modals/fighterDetails';
import { createElement } from './helpers/domHelper';
import { fight } from './fight';
import { showWinnerModal } from './modals/winner';
import { IFighterDetails } from './fighter';
import { getFighterDetails } from './services/fightersService';


export function createFighters(fighters: IFighterDetails[]): HTMLElement {
  const selectFighterForBattle: (event: Event, fighter: IFighterDetails) => Promise<void> = createFightersSelector();
  const fighterElements: HTMLElement[] = fighters.map(fighter => createFighter(fighter, showFighterDetails, selectFighterForBattle));
  const attributes: { [index: string]: string } = { id: 'fighters-view' };
  const fightersContainer: HTMLElement = createElement({ tagName: 'div', className: 'fighters', attributes: attributes });

  fightersContainer.append(...fighterElements);

  return fightersContainer;
}

const fightersDetailsCache: Map<string, IFighterDetails> = new Map<string, IFighterDetails>();

async function showFighterDetails(event: Event, fighter: IFighterDetails): Promise<void> {
  const fullInfo: IFighterDetails = await getFighterInfo(fighter._id);
  showFighterDetailsModal(fullInfo);
}

export async function getFighterInfo(fighterId: string): Promise<IFighterDetails> {
  if (!fightersDetailsCache.has(fighterId)) {
    const fighter: IFighterDetails = await getFighterDetails(fighterId);
    fightersDetailsCache.set(fighterId, fighter);
  }

  return fightersDetailsCache.get(fighterId);
}

function createFightersSelector(): (event: Event, fighter: IFighterDetails) => Promise<void> {
  const selectedFighters: Map<string, IFighterDetails> = new Map<string, IFighterDetails>();

  return async function selectFighterForBattle(event: Event, fighter: IFighterDetails) {
    const fullInfo = await getFighterInfo(fighter._id);

    if ((<HTMLInputElement>(event.target)).checked) {
      selectedFighters.set(fighter._id, fullInfo);
    } else {
      selectedFighters.delete(fighter._id);
    }

    if (selectedFighters.size === 2) {
      const fighters: IterableIterator<IFighterDetails> = selectedFighters.values();
      const winner = await fight(fighters.next().value, fighters.next().value);

      showWinnerModal(winner);

      clearAllCkeckBoxes();
      selectedFighters.clear();
    }
  }
}

function clearAllCkeckBoxes(): void {
  var checkboxes: HTMLInputElement[] = Array.from(document.getElementsByTagName('input'));
  checkboxes.forEach(checkbox => checkbox.checked = false);
}
