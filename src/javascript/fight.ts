import { Fighter, IFighterDetails } from './fighter';
import FightView from "./fightView";

export async function fight(firstFighterInfo: IFighterDetails, secondFighterInfo: IFighterDetails): Promise<IFighterDetails> {

  var firstFighter: Fighter = new Fighter(firstFighterInfo);
  var secondFighter: Fighter = new Fighter(secondFighterInfo);

  const fightView: FightView = new FightView(firstFighter, secondFighter);

  const rootElement = document.getElementById('root') as HTMLElement;

  const fightersViewElement = document.getElementById('fighters-view') as HTMLElement;
  fightersViewElement.style.display = 'none';

  rootElement.appendChild(fightView.element);
  var winner: IFighterDetails = await fightView.beginFight();

  fightersViewElement.style.display = 'flex';
  const arenaElement = document.getElementById('arena') as HTMLElement;
  arenaElement.remove()

  return winner;
}