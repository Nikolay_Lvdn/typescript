import { callApi } from '../helpers/apiHelper';
import { IFighterDetails } from '../fighter'

export async function getFighters(): Promise<IFighterDetails[]> {
  try {
    const endpoint:string = 'fighters.json';
    const apiResult = await callApi(endpoint, 'GET');

    return <IFighterDetails[]>apiResult;
  } catch (error) {
    throw error;
  }
}

export async function getFighterDetails(id: number | string): Promise<IFighterDetails> {
  try {
    const endpoint = `details/fighter/${id}.json`;
    const apiResult: IFighterDetails = <IFighterDetails>(await callApi(endpoint, 'GET'));

    return apiResult;
  }
  catch (error) {
    throw error;
  }
}

