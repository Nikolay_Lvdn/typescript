import { IFighter } from '../fighter'
import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter: IFighter): void {
  const title: string = 'Winner!';

  const { name, source } = fighter;

  const fighterDetails: HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement: HTMLElement = createElement({ tagName: 'p', className: 'fighter-name' });
  const attributes: { src: string } = { src: source };
  const imageElement: HTMLElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: attributes });

  nameElement.innerText = name;

  fighterDetails.append(nameElement);
  fighterDetails.append(imageElement);

  showModal({ title, bodyElement: fighterDetails});
}