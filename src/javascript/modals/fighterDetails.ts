import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';
import { IFighterDetails } from '../fighter';

export function showFighterDetailsModal(fighter: IFighterDetails): void {
  const title: string = 'Fighter info';
  const bodyElement: HTMLElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter: IFighterDetails): HTMLElement {
  const { name, attack, defense, health, source } = fighter;

  const fighterDetails: HTMLElement = createElement({ tagName: 'div', className: 'modal-body' });
  const nameElement: HTMLElement = createElement({ tagName: 'p', className: 'fighter-name' });
  const attackElement: HTMLElement = createElement({ tagName: 'p', className: 'fighter-attack' });
  const defenseElement: HTMLElement = createElement({ tagName: 'p', className: 'fighter-defense' });
  const healthElement: HTMLElement = createElement({ tagName: 'p', className: 'fighter-health' });
  const attributes: { src: string } = { src: source };
  const imageElement: HTMLElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: attributes });

  nameElement.innerText = `Name: ${name}`;
  attackElement.innerText = `Attack: ${attack}`;
  defenseElement.innerText = `Defense: ${defense}`;
  healthElement.innerText = `Health: ${health}`;

  fighterDetails.append(nameElement);
  fighterDetails.append(attackElement);
  fighterDetails.append(defenseElement);
  fighterDetails.append(healthElement);
  fighterDetails.append(imageElement);

  return fighterDetails;
}
