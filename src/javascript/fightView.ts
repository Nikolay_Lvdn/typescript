import { IView, createElement } from './helpers/domHelper';
import { Fighter, IFighterDetails } from "./fighter";

export default class FightView {

    firstFighter: Fighter;
    secondFighter: Fighter;

    element: HTMLElement;
    firstFighterHealthView: HTMLElement;
    secondFighterHealthView: HTMLElement;
    firstFighterImage: HTMLElement;
    secondFighterImage: HTMLElement;

    constructor(firstFighter: Fighter, secondFighter: Fighter) {
        this.firstFighter = firstFighter;
        this.secondFighter = secondFighter;
        this.showArena();
    }

    private showArena() {
        this.firstFighterHealthView = this.createHealthBar(this.firstFighter.health);
        this.secondFighterHealthView = this.createHealthBar(this.secondFighter.health);

        this.firstFighterImage = this.createImage({ src: this.firstFighter.source });
        this.secondFighterImage = this.createImage({ src: this.secondFighter.source, style: "transform: scale(-1, 1);" });

        this.element = createElement({ tagName: 'div', className: 'fighters', attributes: { class: 'battle fighters', id: 'arena' } });

        this.element.append(
            this.firstFighterHealthView,
            this.secondFighterHealthView,
            this.firstFighterImage,
            this.secondFighterImage)
    }

    private createHealthBar(health: number): HTMLElement {
        const attributes = { max: `${health}`, value: `${health}` };
        const healthBar: HTMLElement = createElement({ tagName: 'progress', className: 'progress-bar', attributes })

        return healthBar;
    }

    private createImage(attributes: { [index: string]: string }): HTMLElement {
        const imgElement: HTMLElement = createElement(<IView>{ tagName: 'img', lassName: 'fighter-image', attributes });

        return imgElement;
    }

    public async beginFight(): Promise<IFighterDetails> {
        const attackIntervalInMilliseconds = 500;

        while (true) {
            await new Promise(f => setTimeout(f, attackIntervalInMilliseconds));

            this.firstFighter.getDamage(this.secondFighter);
            this.firstFighterHealthView.setAttribute('value', this.firstFighter.health.toString());

            if (this.firstFighter.health <= 0) {
                return this.secondFighter;
            }
                
            this.secondFighter.getDamage(this.firstFighter);
            this.secondFighterHealthView.setAttribute('value', this.secondFighter.health.toString())

            if (this.secondFighter.health <= 0) {
                return this.firstFighter;
            }
        }
    }
}