export interface IView {
    tagName: string;
    className?: string;
    attributes?: {
        [index: string]: string
    };
}

export function createElement({ tagName, className, attributes = {} }: IView): HTMLElement {
    const element: HTMLElement = document.createElement(tagName);

    if (className) {
        element.classList.add(className);
    }

    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));

    return element;
}