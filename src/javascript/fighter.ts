export interface IFighter {
    _id: string,
    name: string,
    source: string
}

export interface IFighterDetails extends IFighter {
    health: number,
    attack: number,
    defense: number
}

export class Fighter implements IFighterDetails {
    _id: string;
    name: string;
    health: number;
    attack: number;
    defense: number;
    source: string;

    constructor(details: IFighterDetails) {
        this._id = details._id;
        this.name = details.name;
        this.health = details.health;
        this.attack = details.attack;
        this.defense = details.defense;
        this.source = details.source;
    }

    get criticalHitChance(): number {
        const max = 2, min = 1;
        
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    get dodgeChance(): number {
        const max = 2, min = 1;

        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public getHitPower(): number {
        return this.attack * this.criticalHitChance;
    }

    public getBlockPower(): number {
        return this.defense * this.dodgeChance;
    }

    public getDamage(attacker: Fighter): void {
        let reduceBy = attacker.getHitPower() - this.getBlockPower();
        if (reduceBy < 0) {
            reduceBy = 0
        };
        this.health = this.health - reduceBy;
    }
}